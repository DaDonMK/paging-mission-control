import java.io.*;
import java.io.File;  
import java.io.FileNotFoundException;  
import java.util.Scanner; 
import java.util.HashMap;



class Telemetry{  
    public static void main(String args[]){  
  	Worker worker = new Worker();
    }  
} 

class Worker
        {
	    
	    HashMap<String, LastViolation> dicMonitor = new HashMap<String, LastViolation>();

            public Worker()
            {
                // set up monitoring table
                InitializeLookupTable();
                MonitorData();
             }

            private void InitializeLookupTable()
            {
                // satellite 1000
                String key = "TSTAT-1000";
                LastViolation lv = new LastViolation();
                lv.timestamp = "";
                lv.violationsCountInFiveMin = 0;
                dicMonitor.put(key, lv);

                // satellite 1001
                key = "TSTAT-1001";
                lv = new LastViolation();
                lv.timestamp = "";
                lv.violationsCountInFiveMin = 0;
                dicMonitor.put(key, lv);

                // battery 1000
                key = "BATT-1000";
                lv = new LastViolation();
                lv.timestamp = "";
                lv.violationsCountInFiveMin = 0;
                dicMonitor.put(key, lv);

                // battery 1001
                key = "BATT-1001";
                lv = new LastViolation();
                lv.timestamp = "";
                lv.violationsCountInFiveMin = 0;
                dicMonitor.put(key, lv);

            }

            private void MonitorData() 
            {
                String line;

	try { 
		FileReader fr=new FileReader("input.txt");    
		BufferedReader br=new BufferedReader(fr);
  
		int line_num = 1;
                while ((line = br.readLine()) != null)
                {
                    // get values from line data
                    String[] fields = line.split("\\|");
                    String timestamp = fields[0];
                    String satelliteid = fields[1];
                    double red_high_limit = Double.parseDouble(fields[2]);
                    double yellow_high_limit = Double.parseDouble(fields[3]);
                    double yellow_low_limit = Double.parseDouble(fields[4]);
                    double red_low_limit = Double.parseDouble(fields[5]);
                    double raw_value = Double.parseDouble(fields[6]);
                    String component = fields[7];

                    if (component.equals("BATT"))
                    {
                        if (raw_value > red_low_limit)
                            continue;   // ignore this line and get next line
                        else
                        {
                            int count = ProcessViolation(component, satelliteid, timestamp);
                            if (count == 3) GenerateAlert(component, satelliteid,  "RED LOW");
                        }
                    }
                    if (component.equals("TSTAT"))
                    {
                        if (raw_value < red_high_limit)
                            continue;   // ignore this line and get next line
                        else
                        {
                            int count = ProcessViolation(component, satelliteid, timestamp);
                            if (count == 3) GenerateAlert(component, satelliteid,  "RED HIGH");
                        }
                    }

                   // System.out.print(line);
                    //counter++;
                } //while

                br.close();
}
catch (Exception e)
{
	System.out.println(e); 
}
            }

            private int ProcessViolation(String component, String ID, String timestamp)
            {
                String key = component + "-" +  ID;
                LastViolation lv = dicMonitor.get(key);

                // case 1: no violations stored
                if (lv.violationsCountInFiveMin == 0)
                {
                    lv.timestamp = timestamp;
                    lv.violationsCountInFiveMin = 1;
                    return lv.violationsCountInFiveMin;
			
		}
                // case 2: one violation stored, but it is older than 5 minutes
                else 
		{

			if (ViolationOlderThanFive(lv.timestamp, timestamp))
                	{
                    		lv.timestamp = timestamp;
                    		lv.violationsCountInFiveMin = 1;
                    		return lv.violationsCountInFiveMin;
                	}
                		// case 3: one violation stored, and it is less than 5 minutes old
                	else
                	{
                    		lv.violationsCountInFiveMin++;
                    		return lv.violationsCountInFiveMin;
                	}
		}
            }

            private Boolean ViolationOlderThanFive(String oldTimestamp, String newTimestamp)
            {
                double  oldMinutes = Double.parseDouble( oldTimestamp.substring(12, 14) );
                double newMinutes = Double.parseDouble(newTimestamp.substring(12, 14));

                if (newMinutes < oldMinutes) newMinutes += 60;
                if ((newMinutes - oldMinutes) > 5.0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            private void GenerateAlert(String component, String satelliteid, String msg)
            {
		String key = component + "-" + satelliteid;
                LastViolation lv = dicMonitor.get(key);


		System.out.println( "{" );
                System.out.println( " \"satelliteId\" : " + satelliteid  + ",");
		System.out.println( " \"severity\" : " + msg + ",");
		System.out.println( " \"component\" : " + component + ",");
		System.out.println( " \"timestamp\" : " + lv.timestamp );
		System.out.println( "}" );
                
                // reset monitor
                dicMonitor.get(key).violationsCountInFiveMin = 0;
            }

        } //Worker

        class LastViolation
        {
            public String timestamp;
            public int violationsCountInFiveMin;
        }
 
